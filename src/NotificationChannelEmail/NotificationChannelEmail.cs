﻿using Coscine.Configuration;
using Coscine.NotificationChannelBase;
using System.Threading.Tasks;

namespace Coscine.NotificationChannelEmail
{
    public class NotificationChannelEmail : INotificationChannel
    {
        public NotificationChannelEmail()
        {
        }

        public async Task SendAsync(IConfiguration configuration, Message message)
        {
            if (message.User.EmailAddress == null ||
                !message.MessageData.ContainsKey("subject") ||
                !message.MessageData.ContainsKey("body"))
            {
                return;
            }

            using (Email.Email mail = new Email.Email(
                configuration.GetString("coscine/global/email/port"),
                configuration.GetString("coscine/global/email/host"),
                null,
                null))
            {
                await mail.SendEmailAsync(
                    configuration.GetString("coscine/global/email/from"),
                    message.User.EmailAddress,
                    null,
                    message.MessageData["subject"].ToString(),
                    message.MessageData["body"].ToString());
                return;
            }
        }
    }
}
